﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class FootStepCurveJim : MonoBehaviour
{
    [FMODUnity.EventRef] public string FootstepEvent; //Обращаемя к ФМОД   

    public GameObject FootLeft;
    public GameObject FootRight;
    public Animator JimAnimator;
    public bool LeftIsPlaying;
    public bool RightIsPlaying;

    public LayerMask sloy;
    public float tipPoverhnosti;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()

    {
        //Debug.Log(JimAnimator.GetFloat("footLeft"));
        if (JimAnimator.GetFloat("footLeft") > 0.01 && !LeftIsPlaying) //значение кривой
        {
            ProverkaPoverhnosti(FootLeft);
            FMOD.Studio.EventInstance FootLeftInstance = FMODUnity.RuntimeManager.CreateInstance(FootstepEvent);
            FootLeftInstance.setParameterByName("Surface", tipPoverhnosti);
            FootLeftInstance.setParameterByName("foot_velocity", JimAnimator.GetFloat("footLeft") * 100);
            FootLeftInstance.setParameterByName("InputMagnitude", JimAnimator.GetFloat("InputMagnitude"));
            FootLeftInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(FootLeft)); //левая нога звучит из левой ноги
            FootLeftInstance.start(); //запуск
            FootLeftInstance.release(); //затухание

            LeftIsPlaying = true;
        }
        else if (JimAnimator.GetFloat("footLeft") == 0) LeftIsPlaying = false;


        if (JimAnimator.GetFloat("footRight") > 0.01 && !RightIsPlaying)
        {
            FMOD.Studio.EventInstance FootRightInstance = FMODUnity.RuntimeManager.CreateInstance(FootstepEvent);
            FootRightInstance.setParameterByName("Surface", tipPoverhnosti);
            FootRightInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(FootRight));
            FootRightInstance.setParameterByName("foot_velocity", JimAnimator.GetFloat("footRight") * 100);
            FootRightInstance.setParameterByName("InputMagnitude", JimAnimator.GetFloat("InputMagnitude"));
            FootRightInstance.start();
            FootRightInstance.release();
            RightIsPlaying = true;
        }
        else if (JimAnimator.GetFloat("footRight") == 0) RightIsPlaying = false;


    }

    void ProverkaPoverhnosti(GameObject FootLeft)
    {
        RaycastHit luch;
        if (Physics.Raycast(FootLeft.transform.position, Vector3.down, out luch, 0.3f, sloy)) ; //0.3f -  длинна луча
        {
            //Debug.Log(luch.collider.tag);
            if (luch.collider.CompareTag("vAsphalt")) tipPoverhnosti = 0;
            else if (luch.collider.CompareTag("vCarpet")) tipPoverhnosti = 1;
            else if (luch.collider.CompareTag("vConcrete")) tipPoverhnosti = 2;
            else if (luch.collider.CompareTag("vGrass")) tipPoverhnosti = 3;
            else if (luch.collider.CompareTag("vMetal")) tipPoverhnosti = 4;
            else if (luch.collider.CompareTag("vWood")) tipPoverhnosti = 5;
            else if (luch.collider.CompareTag("vSand")) tipPoverhnosti = 6;
            else if (luch.collider.CompareTag("vEdge")) tipPoverhnosti = 7;
            else if (luch.collider.CompareTag("vFence")) tipPoverhnosti = 8;
            else if (luch.collider.CompareTag("vSmall_Rock")) tipPoverhnosti = 9;
            else tipPoverhnosti = 0;
        }
           


    }
}