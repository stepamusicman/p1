﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;


public class LandingEvent : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string landEvent;

    FMOD.Studio.EventInstance landInstance;

    public LayerMask sloy;
    public float tipPoverhnosti;




    void Start()
    {
        

    }

    void land_low()

    {
        ProverkaPoverhnosti(gameObject);
        landInstance = FMODUnity.RuntimeManager.CreateInstance(landEvent);
        
        landInstance.setParameterByName("Surface", tipPoverhnosti);
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(landInstance, gameObject.transform, gameObject.GetComponent<Rigidbody>());
        landInstance.start();
        landInstance.release();
        
        Debug.Log("Landing Low");


    }

    void land_high()
    {
        ProverkaPoverhnosti(gameObject);
        landInstance = FMODUnity.RuntimeManager.CreateInstance(landEvent);
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(landInstance, gameObject.transform, gameObject.GetComponent<Rigidbody>());
        
        landInstance.setParameterByName("Surface", tipPoverhnosti);
        landInstance.start();
        landInstance.release();
        
        Debug.Log("Landing High");
    }
    void ProverkaPoverhnosti(GameObject gameObject)
    {
        RaycastHit luch;
        if (Physics.Raycast(gameObject.transform.position, Vector3.down, out luch, 0.3f, sloy)) ; //0.3f -  длинна луча
        {
              Debug.Log(luch.collider.tag);
            if (luch.collider.CompareTag("vAsphalt")) tipPoverhnosti = 0;
            else if (luch.collider.CompareTag("vCarpet")) tipPoverhnosti = 1;
            else if (luch.collider.CompareTag("vConcrete")) tipPoverhnosti = 2;
            else if (luch.collider.CompareTag("vGrass")) tipPoverhnosti = 3;
            else if (luch.collider.CompareTag("vMetal")) tipPoverhnosti = 4;
            else if (luch.collider.CompareTag("vWood")) tipPoverhnosti = 5;
            else if (luch.collider.CompareTag("vSand")) tipPoverhnosti = 6;
            else if (luch.collider.CompareTag("vEdge")) tipPoverhnosti = 7;
            else if (luch.collider.CompareTag("vFence")) tipPoverhnosti = 8;
            else if (luch.collider.CompareTag("vSmall_Rock")) tipPoverhnosti = 9;
            else tipPoverhnosti = 0;
        }



    }
}
