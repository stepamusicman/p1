﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;

public class BreathEvent : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string breathEvent;
    FMOD.Studio.EventInstance breathInstance;
    vThirdPersonInput tpInput;
    vThirdPersonController tpController;

    void Start()
    {

        tpInput = GetComponent<vThirdPersonInput>();
        tpController = GetComponent<vThirdPersonController>();
        breathInstance = FMODUnity.RuntimeManager.CreateInstance(breathEvent);
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(breathInstance, gameObject.transform, gameObject.GetComponent<Rigidbody>());
        breathInstance.start();


    }
    void footstep()
    {
        if (tpInput.cc.inputMagnitude > 0.1)
        {


            if (tpController.isJumping == false)
            {


                if (tpController.isSprinting)
                {


                    breathInstance.setParameterByName("locomotion_type", 2f);

                    Debug.Log("Run breath");
                }

                else
                {

                    breathInstance.setParameterByName("locomotion_type", 1f);
                    Debug.Log("walk breath");
                }
            }

        }

    }
    void jump()
    {
        if (tpController.isJumping)
        {

            breathInstance.setParameterByName("locomotion_type", 3f);

        }
    }


    void jump_move()
    {
        if (tpController.isJumping)
        {
            breathInstance.setParameterByName("locomotion_type", 4f);

        }
    }

    void land_low()
    {


        breathInstance.setParameterByName("locomotion_type", 5f);



    }

    void land_high()
    {


        breathInstance.setParameterByName("locomotion_type", 6f);


    }

}
