﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sound_carfire : MonoBehaviour
{
    [FMODUnity.EventRef] public string carfire;
    // Start is called before the first frame update
    void Start()
    {
        FMODUnity.RuntimeManager.PlayOneShot(carfire, gameObject.transform.position);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
