﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LandEvent : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string landevent;
    public Animator JimAnimator;
    public LayerMask sloy;
    public float tipPoverhnosti;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void land()
    {
        ProverkaPoverhnosti();
        FMOD.Studio.EventInstance landInstance = FMODUnity.RuntimeManager.CreateInstance(landevent);
        landInstance.set3DAttributes(FMODUnity.RuntimeManager.To3DAttributes(transform.position));
        landInstance.setParameterByName("Surface", tipPoverhnosti);
        landInstance.start();
        landInstance.release();



    }
    void ProverkaPoverhnosti()
    {
        RaycastHit luch;
        if (Physics.Raycast(transform.position, Vector3.down, out luch, 0.3f, sloy)) ; //0.3f -  длинна луча
        {
            //Debug.Log(luch.collider.tag);
            if (luch.collider.CompareTag("vAsphalt")) tipPoverhnosti = 0;
            else if (luch.collider.CompareTag("vCarpet")) tipPoverhnosti = 1;
            else if (luch.collider.CompareTag("vConcrete")) tipPoverhnosti = 2;
            else if (luch.collider.CompareTag("vGrass")) tipPoverhnosti = 3;
            else if (luch.collider.CompareTag("vMetal")) tipPoverhnosti = 4;
            else if (luch.collider.CompareTag("vWood")) tipPoverhnosti = 5;
            else if (luch.collider.CompareTag("vSand")) tipPoverhnosti = 6;
            else if (luch.collider.CompareTag("vEdge")) tipPoverhnosti = 7;
            else if (luch.collider.CompareTag("vFence")) tipPoverhnosti = 8;
            else if (luch.collider.CompareTag("vSmall_Rock")) tipPoverhnosti = 9;
            else tipPoverhnosti = 0;
        }



    }
}
