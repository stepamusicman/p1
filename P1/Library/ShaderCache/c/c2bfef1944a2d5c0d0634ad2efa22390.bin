<Q                           �X  ���(      4                          xlatMtlMain #include <metal_stdlib>
#include <metal_texture>
using namespace metal;

#if !(__HAVE_FMA__)
#define fma(a,b,c) ((a) * (b) + (c))
#endif

#ifndef XLT_REMAP_O
	#define XLT_REMAP_O {0, 1, 2, 3, 4, 5, 6, 7}
#endif
constexpr constant uint xlt_remap_o[] = XLT_REMAP_O;
struct FGlobals_Type
{
    float4 hlslcc_mtx4x4unity_WorldToObject[4];
    float _TriplanarNormal;
    float4 _Normal_ST;
    float _NormalScale;
    float _TriplanarGlobal;
    float _TriplanarDetailNormal;
    float4 _NormalDetail_ST;
    float _NormalDetailScale;
    float _InvertVertexColorR;
    float _VertexMaskContrast;
    float _ShowVertexMaskR;
    float _TriplanarAlbedo;
    float4 _Albedo_ST;
    float4 _AlbedoColor;
    float _TriplanarDetailAlbedo;
    float4 _AlbedoDetail_ST;
    float4 _DetailColor;
    float _TriplanarMetallicSmoothneess;
    float4 _MetallicSmoothness_ST;
    float _Metallic;
    float _TriplanarDetailMetallicSmoothneess;
    float4 _MetallicSmoothnessDetail_ST;
    float _MetallicDetail;
    float _Smoothness;
    float _SmoothnessDetail;
};

struct Mtl_FragmentIn
{
    float2 TEXCOORD0 [[ user(TEXCOORD0) ]] ;
    float4 TEXCOORD1 [[ user(TEXCOORD1) ]] ;
    float4 TEXCOORD2 [[ user(TEXCOORD2) ]] ;
    float4 TEXCOORD3 [[ user(TEXCOORD3) ]] ;
    float4 COLOR0 [[ user(COLOR0) ]] ;
};

struct Mtl_FragmentOut
{
    float4 SV_Target0 [[ color(xlt_remap_o[0]) ]];
    float4 SV_Target1 [[ color(xlt_remap_o[1]) ]];
    float4 SV_Target2 [[ color(xlt_remap_o[2]) ]];
    float4 SV_Target3 [[ color(xlt_remap_o[3]) ]];
};

fragment Mtl_FragmentOut xlatMtlMain(
    constant FGlobals_Type& FGlobals [[ buffer(0) ]],
    sampler sampler_Normal [[ sampler (0) ]],
    sampler sampler_NormalDetail [[ sampler (1) ]],
    sampler sampler_Albedo [[ sampler (2) ]],
    sampler sampler_AlbedoDetail [[ sampler (3) ]],
    sampler sampler_MetallicSmoothness [[ sampler (4) ]],
    sampler sampler_MetallicSmoothnessDetail [[ sampler (5) ]],
    texture2d<float, access::sample > _Normal [[ texture(0) ]] ,
    texture2d<float, access::sample > _NormalDetail [[ texture(1) ]] ,
    texture2d<float, access::sample > _Albedo [[ texture(2) ]] ,
    texture2d<float, access::sample > _AlbedoDetail [[ texture(3) ]] ,
    texture2d<float, access::sample > _MetallicSmoothness [[ texture(4) ]] ,
    texture2d<float, access::sample > _MetallicSmoothnessDetail [[ texture(5) ]] ,
    Mtl_FragmentIn input [[ stage_in ]])
{
    Mtl_FragmentOut output;
    float3 u_xlat0;
    bool2 u_xlatb0;
    float3 u_xlat1;
    float4 u_xlat2;
    float4 u_xlat3;
    bool u_xlatb3;
    float3 u_xlat4;
    float4 u_xlat5;
    int3 u_xlati5;
    float3 u_xlat6;
    int3 u_xlati6;
    float3 u_xlat7;
    float4 u_xlat8;
    float4 u_xlat9;
    float3 u_xlat10;
    float4 u_xlat11;
    int3 u_xlati11;
    float3 u_xlat12;
    int3 u_xlati12;
    bool2 u_xlatb12;
    float3 u_xlat13;
    float4 u_xlat14;
    float3 u_xlat15;
    float3 u_xlat16;
    float3 u_xlat17;
    float3 u_xlat18;
    float3 u_xlat19;
    float3 u_xlat20;
    float3 u_xlat21;
    float u_xlat22;
    float2 u_xlat24;
    float2 u_xlat26;
    float2 u_xlat42;
    bool u_xlatb42;
    float2 u_xlat43;
    bool u_xlatb43;
    float2 u_xlat45;
    float2 u_xlat48;
    float2 u_xlat51;
    float2 u_xlat54;
    float u_xlat63;
    float u_xlat64;
    bool u_xlatb64;
    output.SV_Target0.w = 1.0;
    u_xlatb0.xy = (float2(0.0, 0.0)!=float2(FGlobals._InvertVertexColorR, FGlobals._ShowVertexMaskR));
    u_xlat42.xy = fma(float2(FGlobals._VertexMaskContrast), float2(1.0, -0.5), float2(0.0, 0.5));
    u_xlat42.x = dot(u_xlat42.xy, input.COLOR0.xw);
    u_xlat63 = (-u_xlat42.x) + 1.0;
    u_xlat0.x = (u_xlatb0.x) ? u_xlat63 : u_xlat42.x;
    u_xlat0.x = clamp(u_xlat0.x, 0.0f, 1.0f);
    u_xlat42.xy = fma(input.TEXCOORD0.xy, FGlobals._Albedo_ST.xy, FGlobals._Albedo_ST.zw);
    u_xlat1.xyz = _Albedo.sample(sampler_Albedo, u_xlat42.xy).xyz;
    u_xlatb42 = float(0.0)!=FGlobals._TriplanarAlbedo;
    u_xlat2 = input.TEXCOORD2.wwww * FGlobals.hlslcc_mtx4x4unity_WorldToObject[1].zyxz;
    u_xlat2 = fma(FGlobals.hlslcc_mtx4x4unity_WorldToObject[0].zyxz, input.TEXCOORD1.wwww, u_xlat2);
    u_xlat2 = fma(FGlobals.hlslcc_mtx4x4unity_WorldToObject[2].zyxz, input.TEXCOORD3.wwww, u_xlat2);
    u_xlat2 = u_xlat2 + FGlobals.hlslcc_mtx4x4unity_WorldToObject[3].zyxz;
    u_xlat3 = u_xlat2.wyzw * FGlobals._Albedo_ST.xyxy;
    u_xlat4.xyz = input.TEXCOORD2.zzz * FGlobals.hlslcc_mtx4x4unity_WorldToObject[1].xyz;
    u_xlat4.xyz = fma(FGlobals.hlslcc_mtx4x4unity_WorldToObject[0].xyz, input.TEXCOORD1.zzz, u_xlat4.xyz);
    u_xlat4.xyz = fma(FGlobals.hlslcc_mtx4x4unity_WorldToObject[2].xyz, input.TEXCOORD3.zzz, u_xlat4.xyz);
    u_xlati5.xyz = int3(uint3((float3(0.0, 0.0, 0.0)<u_xlat4.xyz)) * 0xFFFFFFFFu);
    u_xlati6.xyz = int3(uint3((u_xlat4.xyz<float3(0.0, 0.0, 0.0))) * 0xFFFFFFFFu);
    u_xlati5.xyz = (-u_xlati5.xyz) + u_xlati6.xyz;
    u_xlat5.xyz = float3(u_xlati5.xyz);
    u_xlat5.w = 1.0;
    u_xlat3 = u_xlat3 * u_xlat5.xwyw;
    u_xlat6.xyz = _Albedo.sample(sampler_Albedo, u_xlat3.zw).xyz;
    u_xlat3.xyz = _Albedo.sample(sampler_Albedo, u_xlat3.xy).xyz;
    u_xlat63 = abs(u_xlat4.y) + abs(u_xlat4.x);
    u_xlat63 = abs(u_xlat4.z) + u_xlat63;
    u_xlat63 = u_xlat63 + 9.99999975e-06;
    u_xlat7.xyz = abs(u_xlat4.xyz) / float3(u_xlat63);
    u_xlat6.xyz = u_xlat6.xyz * u_xlat7.yyy;
    u_xlat3.xyz = fma(u_xlat3.xyz, u_xlat7.xxx, u_xlat6.xyz);
    u_xlat6.xy = u_xlat2.zy * FGlobals._Albedo_ST.xy;
    u_xlat48.xy = u_xlat5.zw * float2(-1.0, 1.0);
    u_xlat6.xy = u_xlat48.xy * u_xlat6.xy;
    u_xlat8.xyz = _Albedo.sample(sampler_Albedo, u_xlat6.xy).xyz;
    u_xlat3.xyz = fma(u_xlat8.xyz, u_xlat7.zzz, u_xlat3.xyz);
    u_xlat8.y = input.TEXCOORD2.w;
    u_xlat8.z = input.TEXCOORD1.w;
    u_xlat8.xw = input.TEXCOORD3.ww;
    u_xlat9 = u_xlat8 * FGlobals._Albedo_ST.xyxy;
    u_xlat10.x = input.TEXCOORD1.z;
    u_xlat10.y = input.TEXCOORD2.z;
    u_xlat10.z = input.TEXCOORD3.z;
    u_xlati11.xyz = int3(uint3((float3(0.0, 0.0, 0.0)<u_xlat10.xyz)) * 0xFFFFFFFFu);
    u_xlati12.xyz = int3(uint3((u_xlat10.xyz<float3(0.0, 0.0, 0.0))) * 0xFFFFFFFFu);
    u_xlati11.xyz = (-u_xlati11.xyz) + u_xlati12.xyz;
    u_xlat11.xyz = float3(u_xlati11.xyz);
    u_xlat11.w = 1.0;
    u_xlat9 = u_xlat9 * u_xlat11.xwyw;
    u_xlat12.xyz = _Albedo.sample(sampler_Albedo, u_xlat9.zw).xyz;
    u_xlat9.xyz = _Albedo.sample(sampler_Albedo, u_xlat9.xy).xyz;
    u_xlat63 = abs(u_xlat10.y) + abs(u_xlat10.x);
    u_xlat63 = abs(u_xlat10.z) + u_xlat63;
    u_xlat63 = u_xlat63 + 9.99999975e-06;
    u_xlat13.xyz = abs(u_xlat10.xyz) / float3(u_xlat63);
    u_xlat12.xyz = u_xlat12.xyz * u_xlat13.yyy;
    u_xlat9.xyz = fma(u_xlat9.xyz, u_xlat13.xxx, u_xlat12.xyz);
    u_xlat6.x = input.TEXCOORD1.w;
    u_xlat6.y = input.TEXCOORD2.w;
    u_xlat12.xy = u_xlat6.xy * FGlobals._Albedo_ST.xy;
    u_xlat54.xy = u_xlat11.zw * float2(-1.0, 1.0);
    u_xlat12.xy = u_xlat54.xy * u_xlat12.xy;
    u_xlat14.xyz = _Albedo.sample(sampler_Albedo, u_xlat12.xy).xyz;
    u_xlat9.xyz = fma(u_xlat14.xyz, u_xlat13.zzz, u_xlat9.xyz);
    u_xlatb12.xy = (float2(0.0, 0.0)!=float2(FGlobals._TriplanarGlobal, FGlobals._TriplanarDetailNormal));
    u_xlat3.xyz = (u_xlatb12.x) ? u_xlat3.xyz : u_xlat9.xyz;
    u_xlat1.xyz = (bool(u_xlatb42)) ? u_xlat3.xyz : u_xlat1.xyz;
    u_xlat1.xyz = u_xlat1.xyz * FGlobals._AlbedoColor.xyz;
    u_xlat42.xy = fma(input.TEXCOORD0.xy, FGlobals._AlbedoDetail_ST.xy, FGlobals._AlbedoDetail_ST.zw);
    u_xlat3.xyz = _AlbedoDetail.sample(sampler_AlbedoDetail, u_xlat42.xy).xyz;
    u_xlatb42 = float(0.0)!=FGlobals._TriplanarDetailAlbedo;
    u_xlat9 = u_xlat2.wyzw * FGlobals._AlbedoDetail_ST.xyxy;
    u_xlat9 = u_xlat5.xwyw * u_xlat9;
    u_xlat14.xyz = _AlbedoDetail.sample(sampler_AlbedoDetail, u_xlat9.xy).xyz;
    u_xlat9.xyz = _AlbedoDetail.sample(sampler_AlbedoDetail, u_xlat9.zw).xyz;
    u_xlat9.xyz = u_xlat7.yyy * u_xlat9.xyz;
    u_xlat9.xyz = fma(u_xlat14.xyz, u_xlat7.xxx, u_xlat9.xyz);
    u_xlat14.xy = u_xlat2.zy * FGlobals._AlbedoDetail_ST.xy;
    u_xlat14.xy = u_xlat48.xy * u_xlat14.xy;
    u_xlat14.xyz = _AlbedoDetail.sample(sampler_AlbedoDetail, u_xlat14.xy).xyz;
    u_xlat9.xyz = fma(u_xlat14.xyz, u_xlat7.zzz, u_xlat9.xyz);
    u_xlat14 = u_xlat8 * FGlobals._AlbedoDetail_ST.xyxy;
    u_xlat14 = u_xlat11.xwyw * u_xlat14;
    u_xlat15.xyz = _AlbedoDetail.sample(sampler_AlbedoDetail, u_xlat14.zw).xyz;
    u_xlat14.xyz = _AlbedoDetail.sample(sampler_AlbedoDetail, u_xlat14.xy).xyz;
    u_xlat15.xyz = u_xlat13.yyy * u_xlat15.xyz;
    u_xlat14.xyz = fma(u_xlat14.xyz, u_xlat13.xxx, u_xlat15.xyz);
    u_xlat15.xy = u_xlat6.xy * FGlobals._AlbedoDetail_ST.xy;
    u_xlat15.xy = u_xlat54.xy * u_xlat15.xy;
    u_xlat15.xyz = _AlbedoDetail.sample(sampler_AlbedoDetail, u_xlat15.xy).xyz;
    u_xlat14.xyz = fma(u_xlat15.xyz, u_xlat13.zzz, u_xlat14.xyz);
    u_xlat9.xyz = (u_xlatb12.x) ? u_xlat9.xyz : u_xlat14.xyz;
    u_xlat3.xyz = (bool(u_xlatb42)) ? u_xlat9.xyz : u_xlat3.xyz;
    u_xlat3.xyz = fma(u_xlat3.xyz, FGlobals._DetailColor.xyz, (-u_xlat1.xyz));
    u_xlat1.xyz = fma(u_xlat0.xxx, u_xlat3.xyz, u_xlat1.xyz);
    u_xlat21.xyz = (u_xlatb0.y) ? u_xlat0.xxx : u_xlat1.xyz;
    u_xlat1.xy = fma(input.TEXCOORD0.xy, FGlobals._MetallicSmoothness_ST.xy, FGlobals._MetallicSmoothness_ST.zw);
    u_xlat1.xy = _MetallicSmoothness.sample(sampler_MetallicSmoothness, u_xlat1.xy).xw;
    u_xlatb43 = float(0.0)!=FGlobals._TriplanarMetallicSmoothneess;
    u_xlat3 = u_xlat2.wyzw * FGlobals._MetallicSmoothness_ST.xyxy;
    u_xlat3 = u_xlat5.xwyw * u_xlat3;
    u_xlat3.xy = _MetallicSmoothness.sample(sampler_MetallicSmoothness, u_xlat3.xy).xw;
    u_xlat45.xy = _MetallicSmoothness.sample(sampler_MetallicSmoothness, u_xlat3.zw).xw;
    u_xlat45.xy = u_xlat7.yy * u_xlat45.xy;
    u_xlat3.xy = fma(u_xlat3.xy, u_xlat7.xx, u_xlat45.xy);
    u_xlat45.xy = u_xlat2.zy * FGlobals._MetallicSmoothness_ST.xy;
    u_xlat45.xy = u_xlat48.xy * u_xlat45.xy;
    u_xlat45.xy = _MetallicSmoothness.sample(sampler_MetallicSmoothness, u_xlat45.xy).xw;
    u_xlat3.xy = fma(u_xlat45.xy, u_xlat7.zz, u_xlat3.xy);
    u_xlat9 = u_xlat8 * FGlobals._MetallicSmoothness_ST.xyxy;
    u_xlat9 = u_xlat11.xwyw * u_xlat9;
    u_xlat45.xy = _MetallicSmoothness.sample(sampler_MetallicSmoothness, u_xlat9.zw).xw;
    u_xlat9.xy = _MetallicSmoothness.sample(sampler_MetallicSmoothness, u_xlat9.xy).xw;
    u_xlat45.xy = u_xlat13.yy * u_xlat45.xy;
    u_xlat45.xy = fma(u_xlat9.xy, u_xlat13.xx, u_xlat45.xy);
    u_xlat9.xy = u_xlat6.xy * FGlobals._MetallicSmoothness_ST.xy;
    u_xlat9.xy = u_xlat54.xy * u_xlat9.xy;
    u_xlat9.xy = _MetallicSmoothness.sample(sampler_MetallicSmoothness, u_xlat9.xy).xw;
    u_xlat45.xy = fma(u_xlat9.xy, u_xlat13.zz, u_xlat45.xy);
    u_xlat3.xy = (u_xlatb12.x) ? u_xlat3.xy : u_xlat45.xy;
    u_xlat1.xy = (bool(u_xlatb43)) ? u_xlat3.xy : u_xlat1.xy;
    u_xlat1.x = u_xlat1.x * FGlobals._Metallic;
    u_xlat22 = u_xlat1.y * FGlobals._Smoothness;
    u_xlat43.xy = fma(input.TEXCOORD0.xy, FGlobals._MetallicSmoothnessDetail_ST.xy, FGlobals._MetallicSmoothnessDetail_ST.zw);
    u_xlat43.xy = _MetallicSmoothnessDetail.sample(sampler_MetallicSmoothnessDetail, u_xlat43.xy).xw;
    u_xlatb3 = float(0.0)!=FGlobals._TriplanarDetailMetallicSmoothneess;
    u_xlat9 = u_xlat2 * FGlobals._MetallicSmoothnessDetail_ST.xyxy;
    u_xlat9 = u_xlat5.xwyw * u_xlat9;
    u_xlat24.xy = _MetallicSmoothnessDetail.sample(sampler_MetallicSmoothnessDetail, u_xlat9.zw).xw;
    u_xlat9.xy = _MetallicSmoothnessDetail.sample(sampler_MetallicSmoothnessDetail, u_xlat9.xy).xw;
    u_xlat24.xy = u_xlat7.yy * u_xlat24.xy;
    u_xlat24.xy = fma(u_xlat9.xy, u_xlat7.xx, u_xlat24.xy);
    u_xlat9.xy = u_xlat2.zy * FGlobals._MetallicSmoothnessDetail_ST.xy;
    u_xlat9.xy = u_xlat48.xy * u_xlat9.xy;
    u_xlat9.xy = _MetallicSmoothnessDetail.sample(sampler_MetallicSmoothnessDetail, u_xlat9.xy).xw;
    u_xlat24.xy = fma(u_xlat9.xy, u_xlat7.zz, u_xlat24.xy);
    u_xlat9 = u_xlat8 * FGlobals._MetallicSmoothnessDetail_ST.xyxy;
    u_xlat9 = u_xlat11.xwyw * u_xlat9;
    u_xlat51.xy = _MetallicSmoothnessDetail.sample(sampler_MetallicSmoothnessDetail, u_xlat9.zw).xw;
    u_xlat9.xy = _MetallicSmoothnessDetail.sample(sampler_MetallicSmoothnessDetail, u_xlat9.xy).xw;
    u_xlat51.xy = u_xlat13.yy * u_xlat51.xy;
    u_xlat9.xy = fma(u_xlat9.xy, u_xlat13.xx, u_xlat51.xy);
    u_xlat51.xy = u_xlat6.xy * FGlobals._MetallicSmoothnessDetail_ST.xy;
    u_xlat51.xy = u_xlat54.xy * u_xlat51.xy;
    u_xlat51.xy = _MetallicSmoothnessDetail.sample(sampler_MetallicSmoothnessDetail, u_xlat51.xy).xw;
    u_xlat9.xy = fma(u_xlat51.xy, u_xlat13.zz, u_xlat9.xy);
    u_xlat24.xy = (u_xlatb12.x) ? u_xlat24.xy : u_xlat9.xy;
    u_xlat43.xy = (bool(u_xlatb3)) ? u_xlat24.xy : u_xlat43.xy;
    u_xlat43.x = fma(u_xlat43.x, FGlobals._MetallicDetail, (-u_xlat1.x));
    u_xlat1.x = fma(u_xlat0.x, u_xlat43.x, u_xlat1.x);
    u_xlat43.x = fma(u_xlat43.y, FGlobals._SmoothnessDetail, (-u_xlat22));
    output.SV_Target1.w = fma(u_xlat0.x, u_xlat43.x, u_xlat22);
    u_xlat22 = fma((-u_xlat1.x), 0.779083729, 0.779083729);
    output.SV_Target0.xyz = u_xlat21.xyz * float3(u_xlat22);
    u_xlat21.xyz = u_xlat21.xyz + float3(-0.220916301, -0.220916301, -0.220916301);
    output.SV_Target1.xyz = fma(u_xlat1.xxx, u_xlat21.xyz, float3(0.220916301, 0.220916301, 0.220916301));
    u_xlat1.x = input.TEXCOORD1.z;
    u_xlat3.y = input.TEXCOORD2.z;
    u_xlat21.xy = u_xlat8.zw * FGlobals._Normal_ST.xy;
    u_xlat9 = u_xlat8 * FGlobals._NormalDetail_ST.xyxy;
    u_xlat8.xy = u_xlat8.xy * FGlobals._Normal_ST.xy;
    u_xlat8.xy = u_xlat11.xw * u_xlat8.xy;
    u_xlat8.xyw = _Normal.sample(sampler_Normal, u_xlat8.xy).xyw;
    u_xlat9 = u_xlat11.xwyw * u_xlat9;
    u_xlat21.xy = u_xlat11.yw * u_xlat21.xy;
    u_xlat21.xyz = _Normal.sample(sampler_Normal, u_xlat21.xy).xyw;
    u_xlat21.x = u_xlat21.z * u_xlat21.x;
    u_xlat21.xy = fma(u_xlat21.xy, float2(2.0, 2.0), float2(-1.0, -1.0));
    u_xlat21.xy = u_xlat21.xy * float2(FGlobals._NormalScale);
    u_xlat3.xz = fma(u_xlat21.xy, u_xlat11.yw, u_xlat10.xz);
    u_xlat21.xyz = u_xlat13.yyy * u_xlat3.xyz;
    u_xlat8.z = u_xlat8.w * u_xlat8.x;
    u_xlat3.xy = fma(u_xlat8.yz, float2(2.0, 2.0), float2(-1.0, -1.0));
    u_xlat3.xy = u_xlat3.xy * float2(FGlobals._NormalScale);
    u_xlat1.yz = fma(u_xlat3.xy, u_xlat11.wx, u_xlat10.yz);
    u_xlat21.xyz = fma(u_xlat1.xyz, u_xlat13.xxx, u_xlat21.xyz);
    u_xlat1.z = input.TEXCOORD3.z;
    u_xlat3.xy = u_xlat6.xy * FGlobals._Normal_ST.xy;
    u_xlat3.zw = u_xlat6.xy * FGlobals._NormalDetail_ST.xy;
    u_xlat3 = u_xlat54.xyxy * u_xlat3;
    u_xlat8.xyz = _NormalDetail.sample(sampler_NormalDetail, u_xlat3.zw).xyw;
    u_xlat3.xyz = _Normal.sample(sampler_Normal, u_xlat3.xy).xyw;
    u_xlat3.x = u_xlat3.z * u_xlat3.x;
    u_xlat3.xy = fma(u_xlat3.xy, float2(2.0, 2.0), float2(-1.0, -1.0));
    u_xlat3.xy = u_xlat3.xy * float2(FGlobals._NormalScale);
    u_xlat1.xy = fma(u_xlat3.xy, u_xlat54.xy, u_xlat10.xy);
    u_xlat21.xyz = fma(u_xlat1.xyz, u_xlat13.zzz, u_xlat21.xyz);
    u_xlat1.x = dot(u_xlat21.xyz, u_xlat21.xyz);
    u_xlat1.x = rsqrt(u_xlat1.x);
    u_xlat21.xyz = u_xlat21.xyz * u_xlat1.xxx;
    u_xlat1.z = dot(u_xlat10.xyz, u_xlat21.xyz);
    u_xlat3 = u_xlat2.wyzw * FGlobals._Normal_ST.xyxy;
    u_xlat3 = u_xlat5.xwyw * u_xlat3;
    u_xlat14.xyz = _Normal.sample(sampler_Normal, u_xlat3.zw).xyw;
    u_xlat3.xyw = _Normal.sample(sampler_Normal, u_xlat3.xy).xyw;
    u_xlat14.x = u_xlat14.z * u_xlat14.x;
    u_xlat6.xy = fma(u_xlat14.xy, float2(2.0, 2.0), float2(-1.0, -1.0));
    u_xlat6.xy = u_xlat6.xy * float2(FGlobals._NormalScale);
    u_xlat14.xz = fma(u_xlat6.xy, u_xlat5.yw, u_xlat4.xz);
    u_xlat14.y = u_xlat4.y;
    u_xlat15.xyz = u_xlat7.yyy * u_xlat14.xyz;
    u_xlat3.z = u_xlat3.w * u_xlat3.x;
    u_xlat3.xy = fma(u_xlat3.yz, float2(2.0, 2.0), float2(-1.0, -1.0));
    u_xlat3.xy = u_xlat3.xy * float2(FGlobals._NormalScale);
    u_xlat3.yz = fma(u_xlat3.xy, u_xlat5.wx, u_xlat4.yz);
    u_xlat3.x = u_xlat4.x;
    u_xlat15.xyz = fma(u_xlat3.xyz, u_xlat7.xxx, u_xlat15.xyz);
    u_xlat6.xy = u_xlat2.zy * FGlobals._Normal_ST.xy;
    u_xlat6.xy = u_xlat48.xy * u_xlat6.xy;
    u_xlat16.xyz = _Normal.sample(sampler_Normal, u_xlat6.xy).xyw;
    u_xlat16.x = u_xlat16.z * u_xlat16.x;
    u_xlat6.xy = fma(u_xlat16.xy, float2(2.0, 2.0), float2(-1.0, -1.0));
    u_xlat6.xy = u_xlat6.xy * float2(FGlobals._NormalScale);
    u_xlat16.xy = fma(u_xlat6.xy, u_xlat48.xy, u_xlat4.xy);
    u_xlat16.z = u_xlat4.z;
    u_xlat15.xyz = fma(u_xlat16.xyz, u_xlat7.zzz, u_xlat15.xyz);
    u_xlat64 = dot(u_xlat15.xyz, u_xlat15.xyz);
    u_xlat64 = rsqrt(u_xlat64);
    u_xlat15.xyz = float3(u_xlat64) * u_xlat15.xyz;
    u_xlat17.z = dot(u_xlat4.xyz, u_xlat15.xyz);
    u_xlat18.xyz = input.TEXCOORD2.xxx * FGlobals.hlslcc_mtx4x4unity_WorldToObject[1].xyz;
    u_xlat18.xyz = fma(FGlobals.hlslcc_mtx4x4unity_WorldToObject[0].xyz, input.TEXCOORD1.xxx, u_xlat18.xyz);
    u_xlat18.xyz = fma(FGlobals.hlslcc_mtx4x4unity_WorldToObject[2].xyz, input.TEXCOORD3.xxx, u_xlat18.xyz);
    u_xlat17.x = dot(u_xlat18.xyz, u_xlat15.xyz);
    u_xlat19.xyz = input.TEXCOORD2.yyy * FGlobals.hlslcc_mtx4x4unity_WorldToObject[1].xyz;
    u_xlat19.xyz = fma(FGlobals.hlslcc_mtx4x4unity_WorldToObject[0].xyz, input.TEXCOORD1.yyy, u_xlat19.xyz);
    u_xlat19.xyz = fma(FGlobals.hlslcc_mtx4x4unity_WorldToObject[2].xyz, input.TEXCOORD3.yyy, u_xlat19.xyz);
    u_xlat17.y = dot(u_xlat19.xyz, u_xlat15.xyz);
    u_xlat15.x = input.TEXCOORD1.x;
    u_xlat15.y = input.TEXCOORD2.x;
    u_xlat15.z = input.TEXCOORD3.x;
    u_xlat1.x = dot(u_xlat15.xyz, u_xlat21.xyz);
    u_xlat20.x = input.TEXCOORD1.y;
    u_xlat20.y = input.TEXCOORD2.y;
    u_xlat20.z = input.TEXCOORD3.y;
    u_xlat1.y = dot(u_xlat20.xyz, u_xlat21.xyz);
    u_xlat21.xyz = (u_xlatb12.x) ? u_xlat17.xyz : u_xlat1.xyz;
    u_xlat1.xy = fma(input.TEXCOORD0.xy, FGlobals._Normal_ST.xy, FGlobals._Normal_ST.zw);
    u_xlat1.xyz = _Normal.sample(sampler_Normal, u_xlat1.xy).xyw;
    u_xlat1.x = u_xlat1.z * u_xlat1.x;
    u_xlat1.xy = fma(u_xlat1.xy, float2(2.0, 2.0), float2(-1.0, -1.0));
    u_xlat1.xy = u_xlat1.xy * float2(FGlobals._NormalScale);
    u_xlat64 = dot(u_xlat1.xy, u_xlat1.xy);
    u_xlat64 = min(u_xlat64, 1.0);
    u_xlat64 = (-u_xlat64) + 1.0;
    u_xlat1.z = sqrt(u_xlat64);
    u_xlatb64 = float(0.0)!=FGlobals._TriplanarNormal;
    u_xlat21.xyz = (bool(u_xlatb64)) ? u_xlat21.xyz : u_xlat1.xyz;
    u_xlat1.xyz = _NormalDetail.sample(sampler_NormalDetail, u_xlat9.zw).xyw;
    u_xlat9.xyw = _NormalDetail.sample(sampler_NormalDetail, u_xlat9.xy).xyw;
    u_xlat1.x = u_xlat1.z * u_xlat1.x;
    u_xlat1.xy = fma(u_xlat1.xy, float2(2.0, 2.0), float2(-1.0, -1.0));
    u_xlat1.xy = u_xlat1.xy * float2(FGlobals._NormalDetailScale);
    u_xlat1.xz = fma(u_xlat1.xy, u_xlat11.yw, u_xlat10.xz);
    u_xlat1.y = input.TEXCOORD2.z;
    u_xlat1.xyz = u_xlat13.yyy * u_xlat1.xyz;
    u_xlat9.z = u_xlat9.w * u_xlat9.x;
    u_xlat6.xy = fma(u_xlat9.yz, float2(2.0, 2.0), float2(-1.0, -1.0));
    u_xlat6.xy = u_xlat6.xy * float2(FGlobals._NormalDetailScale);
    u_xlat9.yz = fma(u_xlat6.xy, u_xlat11.wx, u_xlat10.yz);
    u_xlat9.x = input.TEXCOORD1.z;
    u_xlat1.xyz = fma(u_xlat9.xyz, u_xlat13.xxx, u_xlat1.xyz);
    u_xlat8.x = u_xlat8.z * u_xlat8.x;
    u_xlat6.xy = fma(u_xlat8.xy, float2(2.0, 2.0), float2(-1.0, -1.0));
    u_xlat6.xy = u_xlat6.xy * float2(FGlobals._NormalDetailScale);
    u_xlat8.xy = fma(u_xlat6.xy, u_xlat54.xy, u_xlat10.xy);
    u_xlat8.z = input.TEXCOORD3.z;
    u_xlat1.xyz = fma(u_xlat8.xyz, u_xlat13.zzz, u_xlat1.xyz);
    u_xlat64 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat64 = rsqrt(u_xlat64);
    u_xlat1.xyz = float3(u_xlat64) * u_xlat1.xyz;
    u_xlat8.z = dot(u_xlat10.xyz, u_xlat1.xyz);
    u_xlat9 = u_xlat2.wyzw * FGlobals._NormalDetail_ST.xyxy;
    u_xlat2.xy = u_xlat2.zy * FGlobals._NormalDetail_ST.xy;
    u_xlat2.xy = u_xlat48.xy * u_xlat2.xy;
    u_xlat2.xyz = _NormalDetail.sample(sampler_NormalDetail, u_xlat2.xy).xyw;
    u_xlat9 = u_xlat5.xwyw * u_xlat9;
    u_xlat10.xyz = _NormalDetail.sample(sampler_NormalDetail, u_xlat9.zw).xyw;
    u_xlat9.xyw = _NormalDetail.sample(sampler_NormalDetail, u_xlat9.xy).xyw;
    u_xlat10.x = u_xlat10.z * u_xlat10.x;
    u_xlat6.xy = fma(u_xlat10.xy, float2(2.0, 2.0), float2(-1.0, -1.0));
    u_xlat6.xy = u_xlat6.xy * float2(FGlobals._NormalDetailScale);
    u_xlat14.xz = fma(u_xlat6.xy, u_xlat5.yw, u_xlat4.xz);
    u_xlat10.xyz = u_xlat7.yyy * u_xlat14.xyz;
    u_xlat9.z = u_xlat9.w * u_xlat9.x;
    u_xlat26.xy = fma(u_xlat9.yz, float2(2.0, 2.0), float2(-1.0, -1.0));
    u_xlat26.xy = u_xlat26.xy * float2(FGlobals._NormalDetailScale);
    u_xlat3.yz = fma(u_xlat26.xy, u_xlat5.wx, u_xlat4.yz);
    u_xlat3.xyz = fma(u_xlat3.xyz, u_xlat7.xxx, u_xlat10.xyz);
    u_xlat2.x = u_xlat2.z * u_xlat2.x;
    u_xlat2.xy = fma(u_xlat2.xy, float2(2.0, 2.0), float2(-1.0, -1.0));
    u_xlat2.xy = u_xlat2.xy * float2(FGlobals._NormalDetailScale);
    u_xlat16.xy = fma(u_xlat2.xy, u_xlat48.xy, u_xlat4.xy);
    u_xlat2.xyz = fma(u_xlat16.xyz, u_xlat7.zzz, u_xlat3.xyz);
    u_xlat64 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat64 = rsqrt(u_xlat64);
    u_xlat2.xyz = float3(u_xlat64) * u_xlat2.xyz;
    u_xlat3.z = dot(u_xlat4.xyz, u_xlat2.xyz);
    u_xlat3.x = dot(u_xlat18.xyz, u_xlat2.xyz);
    u_xlat3.y = dot(u_xlat19.xyz, u_xlat2.xyz);
    u_xlat8.x = dot(u_xlat15.xyz, u_xlat1.xyz);
    u_xlat8.y = dot(u_xlat20.xyz, u_xlat1.xyz);
    u_xlat1.xyz = (u_xlatb12.x) ? u_xlat3.xyz : u_xlat8.xyz;
    u_xlat2.xy = fma(input.TEXCOORD0.xy, FGlobals._NormalDetail_ST.xy, FGlobals._NormalDetail_ST.zw);
    u_xlat2.xyz = _NormalDetail.sample(sampler_NormalDetail, u_xlat2.xy).xyw;
    u_xlat2.x = u_xlat2.z * u_xlat2.x;
    u_xlat2.xy = fma(u_xlat2.xy, float2(2.0, 2.0), float2(-1.0, -1.0));
    u_xlat2.xy = u_xlat2.xy * float2(FGlobals._NormalDetailScale);
    u_xlat64 = dot(u_xlat2.xy, u_xlat2.xy);
    u_xlat64 = min(u_xlat64, 1.0);
    u_xlat64 = (-u_xlat64) + 1.0;
    u_xlat2.z = sqrt(u_xlat64);
    u_xlat1.xyz = (u_xlatb12.y) ? u_xlat1.xyz : u_xlat2.xyz;
    u_xlat1.xyz = (-u_xlat21.xyz) + u_xlat1.xyz;
    u_xlat0.xyz = fma(u_xlat0.xxx, u_xlat1.xyz, u_xlat21.xyz);
    u_xlat1.x = dot(input.TEXCOORD1.xyz, u_xlat0.xyz);
    u_xlat1.y = dot(input.TEXCOORD2.xyz, u_xlat0.xyz);
    u_xlat1.z = dot(input.TEXCOORD3.xyz, u_xlat0.xyz);
    u_xlat0.x = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat0.x = rsqrt(u_xlat0.x);
    u_xlat0.xyz = u_xlat0.xxx * u_xlat1.xyz;
    output.SV_Target2.xyz = fma(u_xlat0.xyz, float3(0.5, 0.5, 0.5), float3(0.5, 0.5, 0.5));
    output.SV_Target2.w = 1.0;
    output.SV_Target3 = float4(1.0, 1.0, 1.0, 1.0);
    return output;
}
                                FGlobals<        _TriplanarNormal                  @   
   _Normal_ST                    P      _NormalScale                  `      _TriplanarGlobal                  d      _TriplanarDetailNormal                    h      _NormalDetail_ST                  p      _NormalDetailScale                    �      _InvertVertexColorR                   �      _VertexMaskContrast                   �      _ShowVertexMaskR                  �      _TriplanarAlbedo                  �   
   _Albedo_ST                    �      _AlbedoColor                  �      _TriplanarDetailAlbedo                    �      _AlbedoDetail_ST                  �      _DetailColor                  �      _TriplanarMetallicSmoothneess                     �      _MetallicSmoothness_ST                       	   _Metallic                       #   _TriplanarDetailMetallicSmoothneess                        _MetallicSmoothnessDetail_ST                        _MetallicDetail                   0     _Smoothness                   4     _SmoothnessDetail                     8     unity_WorldToObject                                _Normal                   _NormalDetail                   _Albedo                 _AlbedoDetail                   _MetallicSmoothness                 _MetallicSmoothnessDetail                   FGlobals           